const express = require('express');
const router = express.Router();
const moongose = require('mongoose');

const Product = require('../models/products');

router.get('/', (req, res, next) => {
    Product.find()
        .select('name price _id')
        .then(docs => {
            if (!docs) throw new Error('Not Found');

            const responce = {
                count: docs.length,
                products: docs.map(doc => ({
                    ...doc._doc,
                    request: {
                        type: 'GET',
                        url: '/products/' + doc._id
                    }
                }))
            };

            res.json(responce);
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({ error });
        })
});

router.post('/', (req, res, next) => {
    const product = new Product({
        _id: new moongose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });

    product.save()
        .then(result => {
            res.status(201).json({
                message: 'Created',
                createdProduct: {
                    _id: result.id,
                    name: result.name,
                    price: result.price
                }
            });
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({ error });
        })


});

router.get('/:productId', (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id)
        .select('name price _id')
        .then(doc => {
            if (!doc) throw new Error('Not Found');
            const response = {
                ...doc._doc,
                request: {
                    type: 'GET',
                    url: '/products'
                }
            }
            res.json(response)
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({ error });
        })
});

router.patch('/:productId', (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Product.update({ _id: id }, { $set: updateOps })
        .then(doc => {
            console.log(doc);
            if (!doc.n) throw new Error('Not Found');
            res.json({
                message: 'updated',
                request: {
                    type: 'GET',
                    url: '/products/' + id
                },
            })
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({ error });
        })

});

router.delete('/:productId', (req, res, next) => {
    const id = req.params.productId;
    Product.findByIdAndRemove(id)
        .then(result => {
            if(!result)  throw new Error('Not Found');
            res.json({
                message: 'deleted',
                request: {
                    type: 'POST',
                    url: '/products',
                    body: { name: 'String', price: 'Number' }
                }
            });
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({ error });
        });
});


module.exports = router;