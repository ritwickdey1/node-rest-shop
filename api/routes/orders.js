const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'Handling GET Request to /orders'
    });
});

router.post('/', (req, res, next) => {
    const order = {
        productId: req.body.productId,
        quantity: req.body.quantity
    }
    res.status(201).json({
        message: 'Handling post Request to /orders',
        createdOrder: order
    });
});

router.get('/:orderId', (req, res, next) => {
    const id = req.params.orderId;

    if (id === 'special') {
        res.status(200).json({
            orderId: id,
            message: 'Special Id'
        });
    }
    else {
        res.status(200).json({
            orderId: id,
            message: `Oops! You're NOT so Special. ;)`
        });
    }
});

router.patch('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    res.status(200).json({
        message: `Updated order id : ${id}`
    });
});

router.delete('/:orderId', (req, res, next) => {
    const id = req.params.orderId;
    res.status(200).json({
        message: `Deleted order id : ${id}`
    });
});


module.exports = router;