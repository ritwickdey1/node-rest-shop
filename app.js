const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const productRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');

const PASS_DB = process.env.MONGO_PASS;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://ritwickdey_admin:'+ PASS_DB +'@node-shopping-api-shard-00-00-60yut.mongodb.net:27017,node-shopping-api-shard-00-01-60yut.mongodb.net:27017,node-shopping-api-shard-00-02-60yut.mongodb.net:27017/test?ssl=true&replicaSet=node-shopping-api-shard-0&authSource=admin', {
    useMongoClient: true
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, content-type, Accept, Authorization, X-requested-With');

    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE');
        return res.status(200).json({});
    }

    next();
});

app.use(morgan('dev'));

app.use('/products', productRoutes);
app.use('/orders', orderRoutes);

app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message || 'Internal Server Error'
        }
    });
});

module.exports = app;